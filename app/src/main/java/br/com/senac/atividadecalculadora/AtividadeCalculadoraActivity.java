package br.com.senac.atividadecalculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AtividadeCalculadoraActivity extends AppCompatActivity {


    private Button nun1;
    private Button nun2;
    private Button nun3;
    private Button nun4;
    private Button nun5;
    private Button nun6;
    private Button nun7;
    private Button nun8;
    private Button nun9;
    private Button nun0;
    private Button soma;
    private Button subitracao;
    private Button multiplicacao;
    private Button divisao;
    private Button ponto;
    private Button igualdade;
    private Button limpar;
    private TextView textViewDisplay;
    Display display;

    private static final char ADICAO = '+';
    private static final char SUBTRACAO = '-';
    private static final char DIVISAO = '/';
    private static final char MULTIPLICACAO = '*';

    private double operando1 = Double.NaN;
    private double operando2;
    private char operacao;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atividade_calculadora);

        this.nun0 = (Button) findViewById(R.id.nun0);
        this.nun1 = (Button) findViewById(R.id.nun1);
        this.nun2 = (Button) findViewById(R.id.nun2);
        this.nun3 = (Button) findViewById(R.id.nun3);
        this.nun4 = (Button) findViewById(R.id.nun4);
        this.nun5 = (Button) findViewById(R.id.nun5);
        this.nun6 = (Button) findViewById(R.id.nun6);
        this.nun7 = (Button) findViewById(R.id.nun7);
        this.nun8 = (Button) findViewById(R.id.nun8);
        this.nun9 = (Button) findViewById(R.id.nun9);
        this.ponto = (Button) findViewById(R.id.ponto);

        this.textViewDisplay = (TextView) findViewById(R.id.telaR);

        display = new Display(this.textViewDisplay);

        ClickDigito clickDigito = new ClickDigito(display);

        this.nun0.setOnClickListener(clickDigito);
        this.nun1.setOnClickListener(clickDigito);
        this.nun2.setOnClickListener(clickDigito);
        this.nun3.setOnClickListener(clickDigito);
        this.nun4.setOnClickListener(clickDigito);
        this.nun5.setOnClickListener(clickDigito);
        this.nun6.setOnClickListener(clickDigito);
        this.nun7.setOnClickListener(clickDigito);
        this.nun8.setOnClickListener(clickDigito);
        this.nun9.setOnClickListener(clickDigito);

        this.ponto.setOnClickListener(clickDigito);

        this.limpar = (Button) findViewById(R.id.limpar);
        this.limpar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            limparDisplay(true);


            }
        });

        this.soma = (Button) findViewById(R.id.soma);
        this.subitracao = (Button) findViewById(R.id.subtracao);
        this.divisao = (Button) findViewById(R.id.divisao);
        this.multiplicacao = (Button) findViewById(R.id.multiplicacao);
        this.igualdade = (Button) findViewById(R.id.igualdade);

        this.soma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = ADICAO;
                calculado = false;
                calcular();
            }
        });

        this.subitracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = SUBTRACAO;
                calculado = false;
                calcular();
            }
        });

        this.divisao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = DIVISAO;
                calculado = false;
                calcular();
            }
        });

        this.multiplicacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = MULTIPLICACAO;
                calculado = false;
                calcular();
            }
        });
        this.igualdade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setLimpaDisplay(false);
                calculado = true;
                calcular();
            }
        });

    }

    private boolean calculado = false;
    private void calcular(){

        if (Double.isNaN(operando1)){
            operando1 = display.getValue();

            limparDisplay(false);

        }else {

            if (!display.isLimpaDisplay()) {
                if (!calculado) {
                    operando2 = display.getValue();
                }

                switch (operacao) {
                    case ADICAO:
                        operando1 = operando1 + operando2;
                        break;
                    case SUBTRACAO:
                        operando1 = operando1 - operando2;
                        break;
                    case DIVISAO:
                        if (operando2 != 0) {

                            operando1 = operando1 / operando2;

                        } else {

                            Context context = getApplicationContext();
                            String mensagem = "Impossível dividir por zero...";
                            int duracao = Toast.LENGTH_LONG;

                            Toast toast = Toast.makeText(context, mensagem, duracao);
                            toast.show();

                        }


                        break;
                    case MULTIPLICACAO:
                        operando1 = operando1 * operando2;
                        break;
                }

                limparDisplay(false);
                display.setText(operando1);

            }
        }
    }

    private void limparDisplay(boolean zerar){
        if (zerar){
            display.setText("0");
            operando1 = Double.NaN;
            //operando2 = 0;
            calculado = false;
        }
        display.setLimpaDisplay(true);
    }
}

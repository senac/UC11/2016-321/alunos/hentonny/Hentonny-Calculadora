package br.com.senac.atividadecalculadora;


import android.widget.TextView;
import java.text.* ;

/**
 * Created by sala304b on 21/08/2017.
 */

public class Display {

    private TextView textView;
    private boolean limpaDisplay = true;

    NumberFormat nf = NumberFormat.getInstance();

    public Display (TextView display){
        this.textView = display;
    }

    public TextView getTextView(){
        return textView;
    }

    public boolean isLimpaDisplay() {return limpaDisplay;}

    public void setLimpaDisplay (boolean limpaDisplay){this.limpaDisplay = limpaDisplay;}

    public String getText() {return textView.getText().toString();}

    public void setText(String texto) {this.textView.setText(texto);}

    public void setText(double valor) {this.setText(nf.format(valor));}

    public  double getValue(){return Double.parseDouble(this.getText());}

}

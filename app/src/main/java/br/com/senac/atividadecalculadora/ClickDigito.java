package br.com.senac.atividadecalculadora;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by sala304b on 15/08/2017.
 */
public class ClickDigito implements View.OnClickListener {

    private Display telaR;

    public ClickDigito(Display display) {
        this.telaR = display;
    }

    @Override
    public void onClick(View v) {

        String texto = telaR.getText();
        Button botao = (Button) v;
        String digito = botao.getText().toString();

            if (telaR.isLimpaDisplay()) {
                telaR.setText(digito);
                telaR.setLimpaDisplay(false);

            } else {
                if (botao.getId() != R.id.ponto) {
                    telaR.setText(texto + digito);
                } else {
                    if (!texto.contains(".")) {


                        telaR.setText(texto + digito);
                    }
                }
            }
    }
}